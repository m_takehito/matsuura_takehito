package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import chapter6.beans.Coment;
import chapter6.exception.NoRowsUpdatedRuntimeException;
import chapter6.exception.SQLRuntimeException;

public class ComentDao {

	public static List<Coment> getComents(Connection connection) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT *");
			sql.append("FROM coments ");
			sql.append("INNER JOIN users ");
			sql.append("ON userid = users.id ");
			sql.append("ORDER BY created_date DESC  ");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<Coment> ret = toComentList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private static List<Coment> toComentList(ResultSet rs)
			throws SQLException {

		List<Coment> ret = new ArrayList<Coment>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				int userid = rs.getInt("userid");
				int messageid = rs.getInt("messageid");
				String text = rs.getString("text");
				Timestamp created_date = rs.getTimestamp("created_date");
				String name = rs.getString("name");
				int is_deleted = rs.getInt("is_deleted");

				Coment coment = new Coment();
				coment.setId(id);
				coment.setUserid(userid);
				coment.setMessageid(messageid);
				coment.setText(text);
				coment.setCreated_date(created_date);
				coment.setName(name);
				coment.setIs_deleted(is_deleted);

				ret.add(coment);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	public List<Coment> register(Connection connection, Coment coment) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO coments ( ");
			sql.append("id");
			sql.append(",text");
			sql.append(", userid");
			sql.append(", name");
			sql.append(", created_date");
			sql.append(") VALUES (");
			sql.append(" ?"); // id
			sql.append(", ?"); // text
			sql.append(", ?"); // userid
			sql.append(", ?"); // name
			sql.append(", ?"); // created_date
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, coment.getId());
			ps.setString(2, coment.getText());
			ps.setInt(3, coment.getUserid());
			ps.setString(4, coment.getName());
			ps.setDate(5, (Date) coment.getCreated_date());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
		return register(connection, coment);
	}

	public void register(Coment coment) {

		Connection connection = null;
		try {
			connection = getConnection();

			ComentDao comentDao = new ComentDao();
			comentDao.insert(connection, coment);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void insert(Connection connection, Coment coment) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO coments ( ");
			sql.append("userid");
			sql.append(", messageid");
			sql.append(", text");
			sql.append(", created_date");
			sql.append(", is_deleted");
			sql.append(") VALUES (");
			sql.append(" ?"); // userid
			sql.append(", ?"); // messageid
			sql.append(", ?"); // text
			sql.append(", CURRENT_TIMESTAMP"); // created_date
			sql.append(", ?"); // is_deleted
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, coment.getUserid());
			ps.setInt(2, coment.getMessageid());
			ps.setString(3, coment.getText());
			ps.setInt(4, coment.getIs_deleted());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public List<Coment> getComent(Connection connection) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append(
					"SELECT coments.text as text,coments.created_date as created_date,users.name as name ,coments.id as id, coments.userid as userid,coments.messageid as messageid, coments.is_deleted as is_deleted ");
			sql.append("FROM  (coments INNER JOIN users ON coments.userid = users.id) ");
			sql.append("INNER JOIN messages  ");
			sql.append("ON coments.messageid = messages.id  ");
			sql.append("ORDER BY coments.created_date  ");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<Coment> ret = toComentList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public void delete(Connection connection,Coment coment) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE coments set is_deleted = 1");
			sql.append(" WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1,coment.getId() );

			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

}