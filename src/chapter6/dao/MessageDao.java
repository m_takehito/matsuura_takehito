package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import chapter6.beans.Coment;
import chapter6.beans.Message;
import chapter6.exception.SQLRuntimeException;

public class MessageDao {

	public void insert(Connection connection, Message message) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO messages ( ");
			sql.append("userid");
			sql.append(", title");
			sql.append(", text");
			sql.append(", category");
			sql.append(", created_date");
			sql.append(") VALUES (");
			sql.append(" ?"); // userid
			sql.append(", ?"); // title
			sql.append(", ?"); // text
			sql.append(", ?"); // category
			sql.append(", CURRENT_TIMESTAMP"); // created_date
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, message.getUserid());
			ps.setString(2, message.getTitle());
			ps.setString(3, message.getText());
			ps.setString(4, message.getCategory());

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public static List<Message> getMessage(Connection connection) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM messages INNER JOIN users ON userid = users.id ";

			ps = connection.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();
			List<Message> messageList = toMessageList(rs);
			if (messageList.isEmpty() == true) {
				return null;
			} else {
				return messageList;
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private static List<Message> toMessageList(ResultSet rs) throws SQLException {

		List<Message> ret = new ArrayList<Message>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				int userid = rs.getInt("userid");
				String title = rs.getString("title");
				String text = rs.getString("text");
				String category = rs.getString("category");
				Timestamp created_date = rs.getTimestamp("created_date");
				String name = rs.getString("name");

				Message message = new Message();
				message.setId(id);
				message.setUserid(userid);
				message.setTitle(title);
				message.setText(text);
				message.setCategory(category);
				message.setCreatedDate(created_date);
				message.setName(name);

				ret.add(message);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	public static List<Coment> getComents(Connection connection) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM coments ";

			ps = connection.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();
			List<Coment> comentsList = toComentsList(rs);
			if (comentsList.isEmpty() == true) {
				return null;
			} else {
				return comentsList;
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private static List<Coment> toComentsList(ResultSet rs) throws SQLException {

		List<Coment> ret = new ArrayList<Coment>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				int userid = rs.getInt("userid");
				int messageid = rs.getInt("messageid");
				String text = rs.getString("text");
				Timestamp created_date = rs.getTimestamp("created_date");
				int is_deleted = rs.getInt("is_deleted");

				Coment coment = new Coment();
				coment.setId(id);
				coment.setUserid(userid);
				coment.setMessageid(messageid);
				coment.setText(text);
				coment.setCreated_date(created_date);
				coment.setIs_deleted(is_deleted);

				ret.add(coment);
			}
			return ret;
		} finally {
			close(rs);
		}
	}
}