package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Coment;
import chapter6.beans.Message;
import chapter6.beans.User;
import chapter6.exception.NoRowsUpdatedRuntimeException;
import chapter6.service.MessageService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<Message> editMessage = new MessageService().getMessage();
		request.setAttribute("editMessage", editMessage);

		User user = (User) request.getSession().getAttribute("loginUser");
		boolean isShowMessageForm;
		if (user != null) {
			isShowMessageForm = true;
		} else {
			isShowMessageForm = false;
		}

		//9/25PM同ログインゆざーの情報をコメントにつけるか⇒INNER JOINで引っ張りコメント登録時に一緒に登録hidden

		List<Coment> editComents = new MessageService().getComent();

		request.setAttribute("editComents", editComents);
		request.setAttribute("isShowMessageForm", isShowMessageForm);
		request.getRequestDispatcher("/top.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<String> message = new ArrayList<String>();
		HttpSession session = request.getSession();

			if (isValid(request, message) == true) {

				try {

					User user = (User) session.getAttribute("loginUser");

					Coment editComent = new Coment();

					editComent.setMessageid(Integer.parseInt(request.getParameter("messageid")));
					editComent.setUserid(user.getId());
					editComent.setText(request.getParameter("text"));

					new MessageService().register(editComent);
					session.setAttribute("editComent", editComent);

				} catch (NoRowsUpdatedRuntimeException e) {
					session.setAttribute("errorMessages", message);
					response.sendRedirect("./");
				}
			} else {
				session.setAttribute("errorMessages", message);
			}
			response.sendRedirect("./");
		}

	private boolean isValid(HttpServletRequest request, List<String> message) {

		String text = request.getParameter("text");

		if (StringUtils.isEmpty(text)) {
			message.add("本文が入力されていません");
		} else {
			if (!(text.matches(".{1,500}"))) {
				message.add("本文は500文字以内で入力してください");
			}
		}
		// TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
		if (message.size() == 0) {
			return true;
		} else {
			return false;
		}

	}

}