package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.beans.User;
import chapter6.service.MessageService;

@WebServlet(urlPatterns = { "/board" })
public class BoardServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		User user = (User) request.getSession().getAttribute("loginUser");

		boolean isShowMessageForm;
		if (user != null) {
			isShowMessageForm = true;
		} else {
			isShowMessageForm = false;
		}

		List<Message> messages = new MessageService().getMessage();

		request.setAttribute("messages", messages);
		request.setAttribute("isShowMessageForm", isShowMessageForm);

		request.getRequestDispatcher("board.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();

		List<String> messages = new ArrayList<String>();

		if (isValid(request, messages) == true) {

			User user = (User) session.getAttribute("loginUser");

			Message message = new Message();
			message.setTitle(request.getParameter("title"));
			message.setText(request.getParameter("message"));
			message.setCategory(request.getParameter("category"));
			message.setUserid(user.getId());

			new MessageService().register(message);

			response.sendRedirect("./");
		} else {
			//エラー時に1つの項目しか表示されない
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("board");
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String message = request.getParameter("message");
		String title = request.getParameter("title");
		String category = request.getParameter("category");

		if (StringUtils.isEmpty(title) == true) {
			messages.add("タイトルを入力してください");
			return false;
		} else if (!(title.matches("^.{1,30}$"))) {
			messages.add("タイトルは30文字以内で入力してください");
			return false;
		}
		if (StringUtils.isEmpty(message) == true) {
			messages.add("メッセージを入力してください");
			return false;
		} else if (1000 < message.length()) {
			messages.add("1,000文字以下で入力してください");
			return false;
		}
		if (StringUtils.isEmpty(category) == true) {
			messages.add("カテゴリーを入力してください");
			return false;
		} else if (!(category.matches("^.{1,10}$"))) {
			messages.add("カテゴリーは10文字以内で入力してください");
			return false;
		}
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}