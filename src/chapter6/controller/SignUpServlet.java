package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.User;
import chapter6.service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		User user = (User) request.getSession().getAttribute("loginUser");
		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();

		if (user != null) {

			if (user.getPosition() == 1) {

				List<User> editUsers = new UserService().getUsers();
				request.setAttribute("editUsers", editUsers);

				List<User> editBranch = new UserService().getBranch();
				request.setAttribute("editBranch", editBranch);

				List<User> editPosition = new UserService().position();
				request.setAttribute("editPosition", editPosition);

				request.getRequestDispatcher("signup.jsp").forward(request, response);
			} else {
				session.setAttribute("errorMessages", messages);
				request.getRequestDispatcher("signup.jsp").forward(request, response);
				return;
			}
		} else {
			response.sendRedirect("./");
		}
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();

		HttpSession session = request.getSession();
		if (isValid(request, messages) == true) {

			User user = new User();
			user.setLoginid(request.getParameter("loginid"));
			user.setPassword(request.getParameter("password"));
			user.setName(request.getParameter("name"));
			user.setBranch(Integer.parseInt(request.getParameter("branchname")));
			user.setPosition(Integer.parseInt(request.getParameter("positionname")));

			new UserService().register(user);

			response.sendRedirect("management");
		} else {
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("signup");
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String loginid = request.getParameter("loginid");
		String password = request.getParameter("password");
		String checkpassword = request.getParameter("checkpassword");
		String name = request.getParameter("name");

		if (StringUtils.isEmpty(loginid)) {
			messages.add("ログインIDを入力してください");
		} else if (!(loginid.matches("^\\w{6,20}$"))) {
			messages.add("ログインIDは半角英数字6~20字で入力してください");
		}
		if (StringUtils.isEmpty(password)) {
			messages.add("パスワードを入力してください");
		} else if (!(password.matches("[ -~]{6,12}"))) {
			messages.add("パスワードは半角文字6~20字で入力してください");
		} else if (StringUtils.isEmpty(checkpassword)) {
			messages.add("確認用パスワードを入力してください");
		} else if (Integer.parseInt(password) != Integer.parseInt(checkpassword)) {
			messages.add("パスワードが違います");
		}
		if (StringUtils.isEmpty(name)) {
			messages.add("名前を入力してください");
		} else if (!(name.matches(".{1,10}"))) {
			messages.add("名前は10文字以内で入力してください");
		}
		// TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}