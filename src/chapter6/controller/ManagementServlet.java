package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import chapter6.beans.User;
import chapter6.exception.NoRowsUpdatedRuntimeException;
import chapter6.service.UserService;

@WebServlet(urlPatterns = { "/management" })
public class ManagementServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();
		User user = (User) request.getSession().getAttribute("loginUser");

		if (user != null) {
			if (user.getPosition() == 1) {
				//ログインユーザー情報のidを元にDBからユーザー情報取得
				List<User> editUsers = new UserService().getUsers();

				request.setAttribute("editUsers", editUsers);

				request.getRequestDispatcher("management.jsp").forward(request, response);
			} else {
				messages.add("権限がないため、ページを開けません");
				session.setAttribute("errorMessages", messages);
				response.sendRedirect("./");
				return;
			}
		} else {
			response.sendRedirect("./");
		}
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();
		User editAccount = getEditAccount(request);

		try {
			if (editAccount.getIs_Stopped() == 0) {
				new UserService().stop(editAccount);
			} else {
				new UserService().revival(editAccount);
			}

		} catch (NoRowsUpdatedRuntimeException e) {
			session.setAttribute("errorMessages", messages);
			request.getRequestDispatcher("management.jsp").forward(request, response);
			return;
		}

		response.sendRedirect("management");
	}

	private User getEditAccount(HttpServletRequest request)
			throws IOException, ServletException {

		User editAccount = new User();
		editAccount.setId(Integer.parseInt(request.getParameter("id")));
		editAccount.setIs_Stopped(Integer.parseInt(request.getParameter("is_stopped")));

		return editAccount;
	}
}
