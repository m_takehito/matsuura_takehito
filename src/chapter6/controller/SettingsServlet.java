package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.User;
import chapter6.exception.NoRowsUpdatedRuntimeException;
import chapter6.service.UserService;

@WebServlet(urlPatterns = { "/settings" })
public class SettingsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();
		User user = (User) request.getSession().getAttribute("loginUser");

		if (user != null) {
			if (user.getPosition() == 1) {
				//HttpSession session = request.getSession();
				//セッションよりログインユーザーの情報を取得
				//User loginUser = (User) session.getAttribute("loginUser");
				//ログインユーザー情報のidを元にDBからユーザー情報取得
				User editUser = new UserService().getUser(Integer.parseInt(request.getParameter("id")));
				request.setAttribute("editUser", editUser);

				List<User> editBranch = new UserService().getBranch();
				request.setAttribute("editBranch", editBranch);

				List<User> editPosition = new UserService().position();
				request.setAttribute("editPosition", editPosition);

				request.getRequestDispatcher("settings.jsp").forward(request, response);
			} else {
				session.setAttribute("errorMessages", messages);
				request.getRequestDispatcher("management.jsp").forward(request, response);
				return;
			}
		} else {
			response.sendRedirect("./");
		}
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();
		User editUser = getEditUser(request);
		if (isValid(request, messages) == true) {

			try {
				if ((StringUtils.isEmpty(request.getParameter("password")))) {
					User editingUser = new UserService().getUser(Integer.parseInt(request.getParameter("id")));
					String brankpass = editingUser.getPassword();
					editUser.setPassword(brankpass);

					new UserService().passbrank(editUser);

				} else {

					new UserService().update(editUser);
				}

			} catch (NoRowsUpdatedRuntimeException e) {
				messages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
				session.setAttribute("errorMessages", messages);
				request.setAttribute("editUser", editUser);
				request.getRequestDispatcher("settings.jsp").forward(request, response);
				return;
			}

			session.setAttribute("loginid", editUser);
			response.sendRedirect("management");

		} else {
			session.setAttribute("errorMessages", messages);
			request.setAttribute("editUser", editUser);
			request.getRequestDispatcher("settings.jsp").forward(request, response);
		}
	}

	private User getEditUser(HttpServletRequest request)
			throws IOException, ServletException {

		User editUser = new User();
		editUser.setId(Integer.parseInt(request.getParameter("id")));
		editUser.setLoginid(request.getParameter("loginid"));
		editUser.setPassword(request.getParameter("password"));
		editUser.setName(request.getParameter("name"));
		editUser.setBranch(Integer.parseInt(request.getParameter("branchname")));
		editUser.setPosition(Integer.parseInt(request.getParameter("positionname")));
		return editUser;
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String loginid = request.getParameter("loginid");
		String password = request.getParameter("password");
		String checkpassword = request.getParameter("checkpassword");
		String name = request.getParameter("name");

		if (StringUtils.isEmpty(loginid) == true) {
			messages.add("ログインIDを入力してください");
		} else if (!(loginid.matches("^\\w{6,20}$"))) {
			messages.add("ログインIDは半角英数字6~20字で入力してください");
		}
		if (!(StringUtils.isEmpty(password))) {
			if (!(password.matches("^\\w{6,20}"))) {
				messages.add("パスワードは半角文字6~20字で入力してください");
			} else {
				if (StringUtils.isEmpty(checkpassword)) {
					messages.add("確認用パスワードを入力してください");
				} else {
					if (Integer.parseInt(password) != Integer.parseInt(checkpassword)) {
						messages.add("パスワードが違います");
					}
				}
			}
		}
		//データをすでに持たせているから、あとはsetするだけ

		if (StringUtils.isEmpty(name)) {
			messages.add("名前を入力してください");
		} else if (!(name.matches("^.{1,10}$"))) {
			messages.add("名前は10文字以内で入力してください");
		}
		// TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}
