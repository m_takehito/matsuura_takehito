package chapter6.beans;

import java.io.Serializable;
import java.util.Date;

public class Coment implements Serializable {
    private static final long serialVersionUID = 1L;

    private  int id;
    private  int userid;
    private int messageid;
    private  String text;
    private  Date created_date;
    private int is_deleted;
    private  String name;


	public  int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public int getMessageid() {
		return messageid;
	}
	public void setMessageid(int messageid) {
		this.messageid = messageid;
	}
	public  String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public int getIs_deleted() {
		return is_deleted;
	}
	public void setIs_deleted(int is_deleted) {
		this.is_deleted = is_deleted;
	}

	public Date getCreated_date() {
		return created_date;
	}
	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
	public String getStatusChangeLabel() {
		return is_deleted == 0 ? "削除" : "復活";
	}

}