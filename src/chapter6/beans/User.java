package chapter6.beans;

import java.io.Serializable;

public class User implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private String loginid;
    private String password;
    private String name;
    private int branch;
    private int position;
    private int is_stopped;
    private String checkpassword;
    private String branchname;
    private String positionname;
    private int positionid;
    private int branchid;


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLoginid() {
		return loginid;
	}
	public void setLoginid(String loginid) {
		this.loginid = loginid;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getBranch() {
		return branch;
	}
	public void setBranch(int branch) {
		this.branch = branch;
	}
	public int getPosition() {
		return position;
	}
	public void setPosition(int position) {
		this.position = position;
	}
	public int getIs_Stopped() {
		return is_stopped;
	}
	public void setIs_Stopped(int is_stopped) {
		this.is_stopped = is_stopped;
	}
	public String getCheckPassword() {
		return checkpassword;
	}
	public void setCheckPassword(String password) {
		this.password = checkpassword;
	}

	public String getStatusChangeLabel() {
		return is_stopped == 0 ? "停止" : "復活";
	}
	public String getBranchname() {
		return branchname;
	}
	public void setBranchname(String branchname) {
		this.branchname = branchname;
	}
	public String getPositionname() {
		return positionname;
	}
	public void setPositionname(String positionname) {
		this.positionname = positionname;
}
	public int getPositionid() {
		return positionid;
	}
	public void setPositionid(int positionid) {
		this.positionid = positionid;
	}
	public int getBranchid() {
		return branchid;
	}
	public void setBranchid(int branchid) {
		this.branchid = branchid;
	}
	}