package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import chapter6.beans.Coment;
import chapter6.beans.Message;
import chapter6.dao.ComentDao;
import chapter6.dao.MessageDao;

public class MessageService {

	public void register(Message message) {

		Connection connection = null;
		try {
			connection = getConnection();

			MessageDao messageDao = new MessageDao();
			messageDao.insert(connection, message);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public List<Message> getMessage() {
		Connection connection = null;
		try {
			connection = getConnection();

			List<Message> message = MessageDao.getMessage(connection);

			commit(connection);

			return message;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public List<Coment> getComents() {
		Connection connection = null;
		try {
			connection = getConnection();

			List<Coment> coments = ComentDao.getComents(connection);

			commit(connection);

			return coments;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void register(Coment coment) {

		Connection connection = null;
		try {
			connection = getConnection();

			ComentDao comentDao = new ComentDao();
			comentDao.insert(connection, coment);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public List<Coment> getComent() {
		Connection connection = null;
		try {
			connection = getConnection();

			ComentDao comentDao = new ComentDao();
			List<Coment> coment = comentDao.getComent(connection);

			commit(connection);

			return coment;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void deletecoment(Coment coment) {
		Connection connection = null;
		try {
			connection = getConnection();

			ComentDao comentDao = new ComentDao();
			comentDao.delete(connection, coment);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}