<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー管理画面</title>
</head>
<div class="header">
	ユーザー管理画面
	<c:if test="${ empty loginUser }">
		<a href="./">戻る</a>
	</c:if>

	<c:if test="${ not empty loginUser }">
		<div class="main-contents">
			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>
		</div>
		<c:if test="${ loginUser.position != 1 }">
		<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>
		</c:if>
		<c:if test="${ loginUser.position == 1 }">
		<a href="signup">新規登録</a>
		<div class="userinfo">
			<div class="account-name">
				<table class="table" border="3" rules="all">
					<tr>
						<th>ログインID</th>
						<th>名前</th>
						<th>支店名</th>
						<th>部署・役職</th>
						<th>アカウントの停止・復活</th>
					</tr>
					<c:forEach items="${editUsers}" var="editUser">
						<tr>
							<th><span class="loginid"><c:out
										value="${editUser.loginid}" /></span></th>
							<th><a href="settings?id=${editUser.id}"><span
									class="name"><c:out value="${editUser.name}" /></span></a></th>
							<th><span class="branchname"><c:out
										value="${editUser.branchname}" /></span></th>
							<th><span class="positionname"><c:out
										value="${editUser.positionname}" /></span></th>

							<th><form method="post" action="management"	onSubmit="return checkSubmit()">
									<input name="id" value="${editUser.id}" id="id" type="hidden" />
									<span class="is_stopped"><input name="is_stopped"
										value="${editUser.is_Stopped}" type="hidden" /></span>
									<input type="submit" name="name"
										value="${editUser.statusChangeLabel}" onclick='return confirm("アカウントの状態を更新しますか？");'>
								</form> <br /></th>
						</tr>>
            </c:forEach>
				</table>
			</div>
		</div>
	</c:if>
	</c:if>
</div>

<br />
<a href="./">戻る</a>

<div class="copyright">Copyright(c)Matsuura_Takehito</div>

</body>
</html>