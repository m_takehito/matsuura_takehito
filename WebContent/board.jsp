<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>掲示板</title>
</head>
<body>

	<c:if test="${ not empty loginUser }">
		<div class="profile">
			<div class="name">
				<h2>
					新規投稿
				</h2>
			</div>
			<div class="loginid">
				ログインID：<c:out value="${loginUser.loginid}" />
			<div class="name">
				名前：<c:out value="${loginUser.name}" />
			</div><br>
			</div>
			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="messages">
							<li><c:out value="${messages}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>
		</div>
	</c:if>

	<div class="form-area">
		<c:if test="${ isShowMessageForm }">
			<form action="board" method="post">

				タイトル(30字以内)<br /> <input name="title"><br /> 本文（1000字以内）<br />
				<textarea name="message" cols="1000" class="tweet-box"></textarea>
				<br /> カテゴリー(10字以内)<br /> <input name="category"><br /> <input
					type="submit" value="投稿する">
			</form>
		</c:if>
	</div>

<!-- 	<div class="messages">
		<c:forEach items="${messages}" var="message">
			<div class="message">
				<div class="account-name">
					<span class="userid"><c:out value="${message.userid}" /></span>
					<span class="name"><c:out value="${loginUser.name}" /></span>
				</div>
				<div class="title">
					<c:out value="${message.title}" />
				</div>
				<div class="text">
					<c:out value="${message.text}" />
				</div>
				<div class="date">
					<fmt:formatDate value="${message.createdDate}"
						pattern="yyyy/MM/dd HH:mm:ss" />
				</div>
			</div>
		</c:forEach>
	</div>
	 -->
						<a href="./">ホーム</a>
	<div class="copyright">Copyright(c)MatsuuraTakehito</div>
</body>
</html>
