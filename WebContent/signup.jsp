<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー登録</title>
</head>
<body>
	<div class="main-contents">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
		<form action="signup" method="post">
			<br /> <label for="loginid">ログインID</label><br /> <input
				name="loginid" id="loginid" /><br /> <label for="password">パスワード</label><br />
			<input name="password" type="password" id="password" /><br /> <label
				for="checkpassword">確認用パスワード</label><br /> <input
				name="checkpassword" type="password" id="checkpassword" /> <br />
			<label for="name">名前</label><br /> <input name="name" id="name" />
			<br /> <label for="branch">支店名</label><br /> <select
				name="branchname">
				<c:forEach items="${editBranch}" var="editBranch">
					<option value="${editBranch.branchid}">${editBranch.branchname}</option>
				</c:forEach>
			</select> <br /> <label for="positionname">部署・役職</label><br /> <select
				name="positionname">
				<c:forEach items="${editPosition}" var="editPosition">
					<option value="${editPosition.positionid}">${editPosition.positionname}</option>
				</c:forEach>
			</select> <br /> <input type="submit" value="登録" /> <br /> <a
				href="management">戻る</a>
		</form>
		<div class="copyright">Copyright(c)Matsuura_Takehito</div>
	</div>
</body>
</html>