<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>掲示板システム</title>
</head>
<body>
	<c:if test="${ empty loginUser }">
		<a href="login">ログイン</a>
	</c:if>
	<c:if test="${ is_stopped == 1 }">
		アカウントが停止されています。
	</c:if>
	<div class="main-contents">
		<div class="header">
			<c:if test="${ not empty loginUser }">

				<a href="./">ホーム</a>
				<a href="board">掲示板投稿</a>
				<a href="management">ユーザー管理画面</a>
				<a href="logout">ログアウト</a>
			</c:if>
		</div>
		<c:if test="${ not empty loginUser }">
			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>
			<div class="profile">
				<div class="loginid">
					<c:out value="${loginUser.loginid}" />
					<div class="messageinfo">
						<c:forEach items="${editMessage}" var="editMessage">

							<span class="title">タイトル:<c:out
									value="${editMessage.title}" /></span>
							<br>
							<span class="text">本文:<c:out value="${editMessage.text}" /></span>
							<br>
							<span class="category">カテゴリー:<c:out
									value="${editMessage.category}" /></span>
							<br>
							<span class="created_date">投稿日時:<c:out
									value="${editMessage.createdDate}" /></span>
							<br>
							<span class="name">投稿者:<c:out value="${editMessage.name}" /></span>
							<br>

												コメント:
									<c:forEach items="${editComents}" var="editComents">
								<c:if test="${ editComents.messageid == editMessage.id }">
									<c:if test="${ editComents.is_deleted == 0 }">
										<input name="id" value="${editComents.id}" type="hidden"
											id="id" />

										<span class="text"><c:out value="${editComents.text}" /></span>
										<span class="created_date"><c:out
												value="${editComents.created_date}" /></span>
										<span class="name"><c:out value="${editComents.name}" /></span>
										<input name="userid" value="${editComents.userid}"
											type="hidden" id="userid" />
										<input name="messageid" value="${editComents.messageid}"
											type="hidden" id="messageid" />
										<input name="is_deleted" value="${editComents.is_deleted}"
											type="hidden" id="is_deleted" />
										<c:if test="${ loginUser.id == editComents.userid }">
											<form method="post" action="delete">
												<input name="id" value="${editComents.id}" id="id"
													type="hidden" /> <span class="is_deleted"><input
													name="is_deleted" value="${editComents.is_deleted}"
													type="hidden" /></span> <input type="submit" name="name"
													value="削除">

											</form><br>
										</c:if>
									</c:if>
								</c:if>
							</c:forEach>
							<c:if test="${ isShowMessageForm }">
								<form action="./" method="post">
									<input name="messageid" value="${editMessage.id}" type="hidden"
										id="messageid" />
									<textarea name="text" cols="50" rows="10" class="tweet-box"></textarea>
									<br /> <input type="submit" name="name" value="コメントする">
									（500文字まで）
								</form>
							</c:if>
						</c:forEach>
					</div>
				</div>
			</div>
		</c:if>
	</div>
	<div class="copyright">Copyright(c)MatsuuraTakehito</div>
</body>
</html>
